# © 2019 Le Filament (<http://www.le-filament.com>)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

import random
import logging

from odoo.tests import tagged, common

_logger = logging.getLogger(__name__)


@tagged('post_install', '-at_install')
class TestApiRiga(common.TransactionCase):

    def test_read(self):
        print("______ Tests Tantiemo ________")
        partner = self.env['res.partner'].search([
            ['name', '=', 'LeFilament']])
        print(partner)
        
        print("____ ID RIGA Partner")
        partner.write({'id_riga': 6481})
        print(partner.id_riga)
        
        print("____ Financial Tools Partner")
        Finance = self.env['scop.financial.tools']
        print(Finance.search([]))
        
        print("____ Update Financial Tools")
        Finance.import_data()
        print(partner.financial_tools_ids)

