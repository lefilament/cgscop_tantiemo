# © 2019 Le Filament (<http://www.le-filament.com>)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

import csv
import io
import logging
from ftplib import FTP

from odoo import models, fields, tools, exceptions

_logger = logging.getLogger(__name__)


class ScopFinancialTools(models.Model):
    _name = "scop.financial.tools"
    _description = "Financial tools"
    _order = "date_notification desc"

    partner_id = fields.Many2one(
        comodel_name='res.partner',
        string='Organisme',
        domain=[('is_company', '=', True)],
        ondelete='restrict', index=True)
    riga_id = fields.Integer("ID RIGA")
    nom_scop = fields.Char("Nom Scop")
    outil = fields.Char("Type d’outil")
    type_intervention = fields.Char("Type d'intervention",)
    typo_financement = fields.Char("Typologie de financement")
    no_intervention = fields.Char("No intervention", index=True)
    montant_accorde = fields.Float("Montant accordé")
    montant_notifie = fields.Float("Montant notifié")
    montant_verse = fields.Float("Montant versé")
    montant_solde = fields.Float("Solde")
    date_decision = fields.Date("Date décision")
    date_versement = fields.Date("Date Versement")
    date_fin = fields.Date("Date de fin")
    encours_total = fields.Float("Encours total")
    encours_garanti = fields.Float("Encours garanti")
    taux_provision = fields.Float("Taux de provision")
    date_notification = fields.Date("Date de l’accord")
    date_convention = fields.Date("Date convention")

    # ------------------------------------------------------
    # Import données
    # ------------------------------------------------------

    def import_data(self):
        # Get Tantiemo Filename
        config = self.env['tantiemo.config'].sudo().search([
            ['active', '=', True]],
            limit=1)
        filename = (config.path +
                    config.name)
        # Get File
        csv_file = open(filename, 'rt', encoding="ISO-8859-1")
        # Suppression des lignes
        self.search([]).unlink()
        try:
            # Création du lecteur CSV.
            reader = csv.DictReader(csv_file, delimiter=';')
            # Lecture de chaque ligne
            for row in reader:
                vals_financial_tools = row
                # Traitement des valeurs nulles
                self._check_empty(vals_financial_tools)
                # récupération du dossier
                partner_id = self.env['res.partner'].search([
                    ['is_company', '=', True],
                    ['id_riga', '=', vals_financial_tools['riga_id']]])
                # si le dossier n'existe pas, on le crée,
                # si il existe, on le met à jour
                if partner_id:
                    vals_financial_tools.update({'partner_id': partner_id.id})
                    self.create(vals_financial_tools)
        except Exception as e:
            _logger.error(e.__str__())

    def _check_empty(self, dict_value):
        """ Parcours les clés d'un dictionnaire pour vérifier si la valeur
        est vide. Si elle est vide, la valeur est mise à False

        :param dict_value : dictionnaire à traiter

        @return dict_value : dictionnaire sans valeurs vide
        """

        for key, value in dict_value.items():
            if not value:
                dict_value[key] = False
        return dict_value
        for key, value in dict_value.items():
            if not value:
                dict_value[key] = False
