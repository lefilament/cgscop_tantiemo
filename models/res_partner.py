# © 2019 Le Filament (<http://www.le-filament.com>)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

from odoo import models, fields


class TantiemoPartner(models.Model):
    _inherit = "res.partner"

    financial_tools_ids = fields.One2many(
        comodel_name='scop.financial.tools',
        inverse_name='partner_id',
        string='Outils financiers')
