# © 2019 Le Filament (<http://www.le-filament.com>)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

from odoo import models, fields


class CgscopTantiemoConfig(models.Model):
    _name = 'tantiemo.config'
    _description = 'Configuration tantiemo'

    name = fields.Char(string='Nom Fichier')
    path = fields.Char('Path fichier')
    active = fields.Boolean('Actif', default=False)

    def toogle_active_config(self):
        """ Active la connexion sélectionnée et désactive
            toutes les autres connexions
        """
        for item in self.env['tantiemo.config'].search([]):
            item.active = False
        self.active = True

    def import_datas(self):
        self.env['scop.financial.tools'].sudo().import_data()
