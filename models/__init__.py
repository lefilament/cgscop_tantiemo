# © 2019 Le Filament (<http://www.le-filament.com>)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

from . import res_partner
from . import scop_financial_tools
from . import tantiemo_config
