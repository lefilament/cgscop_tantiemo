{
    "name": "CG SCOP - Connecteur Tantiemo",
    "summary": "Import des donnees de Tantiemo",
    "version": "12.0.1.0.1",
    "development_status": "Beta",
    "author": "Le Filament",
    "license": "AGPL-3",
    "application": False,
    "depends": ['cgscop_partner'],
    "data": [
        "security/ir.model.access.csv",
        "views/res_partner.xml",
        "views/tantiemo_config.xml",
        "datas/cron_tantiemo.xml",
    ],
    'installable': True,
    'auto_install': False,
}
