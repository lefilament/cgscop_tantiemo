.. image:: https://img.shields.io/badge/licence-AGPL--3-blue.svg
   :target: http://www.gnu.org/licenses/agpl
   :alt: License: AGPL-3


=============================
CG SCOP - Connecteur Tantiemo
=============================

Description
===========

Ce module permet d'intégrer dans Odoo les informations de Tantiemo mises à disposition via des fichiers CSV.

Il hérite *res.partner* pour ajouter une table *scop.financial.tools*


Fonctionnement
==============

Un cron quotidien permet de lire les fichiers, et pour chaque ligne de créer ou mettre à jour un enregistrement enregistrement.


Usage
=====

Les données suivantes sont présentes dans la table **ir.config.parameter** :

* *filename* : tantiemo.latest.csv
* *path* : path du dossier dans lequel se trouve le fichier CSV sur le serveur **/import**


Credits
=======

Funders
------------

The development of this module has been financially supported by:

    Confédération Générale des SCOP (https://www.les-scop.coop)


Contributors
------------

* Juliana Poudou <juliana@le-filament.com>
* Rémi Cazenave <remi@le-filament.com>
* Benjamin Rivier <benjamin@le-filament.com>


Maintainer
----------

.. image:: https://le-filament.com/images/logo-lefilament.png
   :alt: Le Filament
   :target: https://le-filament.com

This module is maintained by Le Filament
